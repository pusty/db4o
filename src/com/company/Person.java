package com.company;

public class Person extends Cart {
    private String name;
    private String surname;
    private Cart cart;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.cart =  new Cart();
    }

    public void addToCart(Item item){
        this.cart.addToCart(item);
    }
}
