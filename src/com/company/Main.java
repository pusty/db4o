package com.company;

import com.db4o.*;

public class Main {

    public static void main(String[] args) {
        ObjectContainer db = null;
        try {
            db = Db4o.openFile("database.data");
            Store store = new Store();
            Queue queue1 = new Queue();
            Queue queue2 = new Queue();
            Cashbox cashbox1 = new Cashbox(1,queue1);
            Cashbox cashbox2 = new Cashbox(2,queue2);
            store.addCashBox(cashbox1);
            store.addCashBox(cashbox2);
            Item item1 = new Item("testowy item",10,2);
            Item item2 = new Item("testowy ite2",12,10);
            Item item3 = new Item("testowy ite3",1,1);
            Person osoba1 = new Person("Andrzej","Nowak");
            Person osoba2 = new Person("Janina","Duda");
            Person osoba3 = new Person("Jan","Nowacki");
            Person osoba4 = new Person("Anna","Dudek");


            osoba1.addToCart(item1);
            osoba2.addToCart(item3);
            osoba2.addToCart(item2);

            queue1.addToQueue(osoba1);
            queue1.addToQueue(osoba2);
            queue1.addToQueue(osoba3);
            queue2.addToQueue(osoba4);

            db.set(item1);
            db.set(item2);
            db.set(item3);
            db.set(osoba1);
            db.set(osoba2);
            db.set(queue1);
            db.set(queue2);
            db.set(cashbox1);
            db.set(cashbox2);
            db.set(store);
            db.commit();

//            ObjectSet persons = db.get(new Person("Andrzej", null));
//            while (persons.hasNext())
//                System.out.println(persons.next());
        } catch(NullPointerException e){
            e.printStackTrace();
        } finally {
            if (db != null)
                db.close();
        }

    }
}
