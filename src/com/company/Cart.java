package com.company;

import java.util.ArrayList;

public class Cart {
    private ArrayList<Item> Items;
    private int totalweight;
    private int totalprice;

    public Cart() {
        this.totalweight = 0;
        this.Items = new ArrayList<>();
    }
    public void addToCart(Item item){
        this.Items.add(item);
        this.setTotalweight(item.getWeight());
        this.setTotalprice(item.getPrice());
    }
    public void setTotalweight(int totalweight) {
        this.totalweight += totalweight;
    }
    public void setTotalprice(int totalprice) {
        this.totalprice += totalprice;
    }
}
